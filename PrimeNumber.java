class PrimeNumber{
	public static void primeNumber(int n){
		if(n <= 1){
			System.out.println( n + " is not a Prime number!");
			return;
		}
		for(int i = 2 ; i < Math.sqrt(n) ; i++){
			if(n % i == 0){
				System.out.println( n + " is not a Prime number!");
				return;
			}
		}
		System.out.println( n + " is a Prime number!");
	}

	public static void main(String args[]){

		System.out.println("Prime Number Program"); //Changes Made Here!!
		primeNumber(6);
		

	}

}